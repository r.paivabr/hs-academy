import * as request from 'request-promise';
import { Request, Response } from 'express';

const headers = { 'Authorization': 'Basic MTAwMDUyNjI6Mjg4YWM1ZjFiNTM0NDY3Mzk0OTlmNzcxNWZlODZiNWU=' };
const uri = 'https://api.userede.com.br/desenvolvedores/v1/transactions';
const api_key = 'e95bf57fb7752c2456d5180bbceb83d5';
const binuri = `https://api.bincodes.com/bin/json/${api_key}`;

export const getByTid = async (req: Request, res: Response) => {
    try {
        const result = await request.get({ url: `${uri}/${req.params.tid}`, headers });
        // console.log(req.body.user);
        res.send(result);
    } catch (error) {
        // console.log(error);
        res.status(error.statusCode).send({ message: error.error.returnMessage });
    }
};

export const newTransaction = async (req: Request, res: Response) => {
    try {
        const result = await request({ method: 'POST', uri, headers, body: req.body, json: true });
        // console.log(req.body.user);
        res.send(result);
    } catch (error) {
        // console.log(error);
        res.status(error.statusCode).send({ message: error.error.returnMessage });
    }
};

export const getByBin = async (req: Request, res: Response) => {
    try {
        const result = await request.get({ url: `${binuri}/${req.params.bin}` });
        // console.log(req.body.user);
        res.send(result);
    } catch (error) {
        console.log(error);
        res.status(500).send({ message: 'Deu ruim!' });
    }
};
