import { https } from 'firebase-functions';
import { initializeApp } from 'firebase-admin';
import * as express from 'express';
import * as cors from 'cors';
import * as cookieParser from 'cookie-parser';
// import * as bodyParser from "body-parser";

import { validateFirebaseIdToken } from './token';
import { getByBin, getByTid, newTransaction } from './payment';

initializeApp();
const app = express();

app.use(cors({ origin: true }));
app.use(cookieParser());
// app.use(bodyParser.json());
app.use(validateFirebaseIdToken);

app.get('/payment/:tid', getByTid);
app.post('/payment', newTransaction);
app.get('/search/:bin', getByBin);

export const api = https.onRequest(app);
