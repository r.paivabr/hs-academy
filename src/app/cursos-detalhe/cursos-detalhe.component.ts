import { Component, OnInit, ViewChild, ElementRef, ChangeDetectorRef, AfterViewInit, OnDestroy, HostListener } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FirestoreService } from '../core/firebase/firestore/firestore.service';

@Component({
  selector: 'app-cursos-detalhe',
  templateUrl: './cursos-detalhe.component.html',
  styleUrls: ['./cursos-detalhe.component.scss']
})
export class CursosDetalheComponent implements OnInit, AfterViewInit, OnDestroy {

  @ViewChild('demoYouTubePlayer') demoYouTubePlayer: ElementRef<HTMLDivElement>;
  id: string;
  yid: string;
  lessons: any[] = [];
  selectedLesson: any;
  videoWidth: number;
  videoHeight: number;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private changeDetectorRef: ChangeDetectorRef,
              private firestore: FirestoreService) { }

  ngOnInit(): void {

    this.id = this.route.snapshot.params.id;
    this.yid = this.route.snapshot.params.yid;
    this.firestore.getAll('lessons').subscribe(lessons => {
      this.lessons = lessons.filter(lesson => lesson.course === this.id);
      if (!this.yid && this.lessons.length > 0) {
        this.router.navigate(['cursos', this.id, this.lessons[0].youtubeId]);
      }
    });
  }

  ngAfterViewInit(): void {
    console.log('bla');
    this.onResize();
    window.addEventListener('resize', this.onResize);
  }

  ngOnDestroy(): void {
    window.removeEventListener('resize', this.onResize);
  }

  openLesson(youtubeId: string) {
    this.router.navigate(['cursos', this.id, youtubeId]);
    this.yid = youtubeId;
  }

  stateChange(event) {
    console.log(event);
  }

  ready(event) {
    console.log(event);
    // setTimeout(() => event.target.playVideo(), );
  }

  onResize = (): void => {
    console.log('youtube:', this.demoYouTubePlayer.nativeElement.clientWidth);
    // console.log('card:', this.card.nativeElement.clientWidth);
    // this.videoWidth = this.card.nativeElement.clientWidth;
    this.videoWidth = Math.min(this.demoYouTubePlayer.nativeElement.clientWidth - 80, 720);
    this.videoHeight = this.videoWidth * 0.6;
    this.changeDetectorRef.detectChanges();
  }

}
