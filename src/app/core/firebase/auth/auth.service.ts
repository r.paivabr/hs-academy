import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';
import { from, Observable, of } from 'rxjs';
import { map, switchMap, tap } from 'rxjs/operators';
import { FirestoreService } from '../firestore/firestore.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  user$: Observable<firebase.User>;
  token$: Observable<string>;
  adminUser$: Observable<any>;

  constructor(private afAuth: AngularFireAuth,
              private firestore: FirestoreService,
              private router: Router) {
    this.user$ = this.afAuth.authState;
    this.token$ = this.afAuth.idToken;
    this.adminUser$ = this.afAuth.authState.pipe(
      switchMap(user => {
        if (user) {
          return this.firestore.getByAdmin(user.uid);
        } else {
          return of(null);
        }
      })
    );
  }

  login(email: string, password: string): Observable<firebase.auth.UserCredential> {
    return from(this.afAuth
      .auth
      .signInWithEmailAndPassword(email, password)
    );
  }

  logout(): void {
    this.afAuth.auth.signOut();
    // this.router.navigateByUrl('login');
  }

  isLoggedIn(): Observable<boolean> {
    return this.user$.pipe(
      map(user => !!user)
    );
  }

  isAdmin(): Observable<boolean> {
    return this.adminUser$.pipe(
      map(admin => !!admin)
    );
  }

  register(email: string, password: string): Observable<firebase.auth.UserCredential> {
    return from(this.afAuth
      .auth
      .createUserWithEmailAndPassword(email, password)
    ).pipe(
      tap(res => {
        if (res) {
        const { emailVerified, uid } = res.user;
        this.firestore.add({email, emailVerified, uid, roles: ['user'] }, 'users');
        }
      })
    );
  }
}
