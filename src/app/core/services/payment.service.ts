import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AuthService } from '../firebase/auth/auth.service';
import { CardResult } from '../models/card-result';
import { map } from 'rxjs/operators';
import { Observable, BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PaymentService {

  private URL = 'https://us-central1-hs-academy-be7ea.cloudfunctions.net/api';
  private APIKEY = 'e95bf57fb7752c2456d5180bbceb83d5';
  private token: string;
  card: CardResult;

  private cardSource = new BehaviorSubject<CardResult>({});
  card$ = this.cardSource.asObservable();

  constructor(private http: HttpClient,
              private auth: AuthService) {
    this.auth.token$.subscribe(token => {
      this.token = token;
      console.log(this.token);
    });
  }

  getToken(): string {
    return this.token;
  }

  getTransactionByTid(tid: string) {
    const headers = new HttpHeaders({ Authorization: `Bearer ${this.token}` });
    return this.http.get<any>(`${this.URL}/payment/${tid}`, { headers });
  }

  createTransaction(transaction: any) {
    const headers = new HttpHeaders({ Authorization: `Bearer ${this.token}` });
    return this.http.post<any>(`${this.URL}/payment`, transaction, { headers });
  }

  test(token: string) {
    const headers = new HttpHeaders({ Authorization: `Bearer ${token}` });
    return this.http.get<any>(`https://us-central1-hs-academy-be7ea.cloudfunctions.net/api/hello`, { headers });
  }

  getByBin(bin: string): void {
    const headers = new HttpHeaders({ Authorization: `Bearer ${this.token}` });
    this.http.get<any>(`${this.URL}/search/${bin}`, { headers }).subscribe(card => {
      console.log(card);
      this.card = {
        bin,
        bank: card.bank || '',
        level: card.level || '',
        type: card.type || '',
        country: card.country || '',
        countryCode: card.countrycode || '',
        valid: card.valid === 'true' ? true : false
      };
      this.cardSource.next(this.card);
      this.getByBinAlt(bin);
    }, err => {
      console.log(err);
      this.getByBinAlt(bin);
    });
  }

  private getByBinAlt(bin: string): void {
    this.http.get<any>(`https://lookup.binlist.net/${bin}`).subscribe(card => {
      console.log(card);
      this.card = {
        ...this.card,
        bin,
        level: card.brand ? card.brand.toUpperCase() : '',
        brand: card.scheme ? card.scheme.toUpperCase() : '',
        type: card.type ? card.type.toUpperCase() : '',
        country: card.country.name ? card.country.name.toUpperCase() : '',
        countryCode: card.country.alpha2 || '',
        currency: card.country.currency || ''
      };
      this.cardSource.next(this.card);
    }, err => console.log(err));
  }

}
