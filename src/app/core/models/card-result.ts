export interface CardResult {
    bin?: string;                // 2 bin
    bank?: string;              // 1 bank.name // 2 bank
    level?: string;              // 1 brand // 2 level
    brand?: string;              // 1 scheme
    type?: string;               // type
    country?: string;            // 1 country.name // 2 country
    countryCode?: string;        // 1 country.alpha2 // 2 countrycode
    currency?: string;           // 1 country.currency
    valid?: boolean;            // 2 valid
}
