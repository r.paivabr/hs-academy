import { NgModule } from '@angular/core';
import { FirebaseModule } from './firebase/firebase.module';
import { MaterialModule } from './material/material.module';

@NgModule({
  imports: [
    FirebaseModule,
    MaterialModule
  ],
  exports: [
    MaterialModule
  ]
})
export class CoreModule { }
