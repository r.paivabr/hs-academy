import { Component, OnInit } from '@angular/core';
import { PaymentService } from '../core/services/payment.service';
import { AuthService } from '../core/firebase/auth/auth.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { CardResult } from '../core/models/card-result';

@Component({
  selector: 'app-teste',
  templateUrl: './teste.component.html',
  styleUrls: ['./teste.component.scss']
})
export class TesteComponent implements OnInit {

  events: string[] = [];
  opened: boolean;
  transacao: any;
  user: firebase.User;
  token: string;
  panelOpenState = false;
  turnCard = false;
  cardNumber = '';
  card: CardResult;
  cardForm: FormGroup;

  constructor(private payment: PaymentService,
              private auth: AuthService) {}

  ngOnInit() {
    this.auth.user$.subscribe(user => {
      console.log(user);
      this.user = user;
    });
    this.auth.token$.subscribe(token => {
      console.log(token);
      this.token = token;
    });
    this.payment.card$.subscribe(card => this.card = card);

    this.cardForm = new FormGroup({
      cardholderName: new FormControl('Rafael Paiva', Validators.required),
      cardNumber: new FormControl('4235647728025682', Validators.required),
      expirationMonth: new FormControl('01', Validators.required),
      expirationYear: new FormControl('2021', Validators.required),
      securityCode: new FormControl('123', Validators.required),
      amount: new FormControl('50', Validators.required),
      installments: new FormControl('1', Validators.required),
      reference: new FormControl('raf1', Validators.required)
    });

  }

  lerTransacao() {
    this.payment.getTransactionByTid('10022002231116130001').subscribe(data => this.transacao = data);
  }

  login() {
    this.auth.login('r.paivabr@gmail.com', '123456').subscribe(
      () => console.log('Logged as r.paivabr@gmail.com'),
      err => console.log(err)
    );
  }

  logout() {
    this.auth.logout();
  }

  cloudFunction() {
    this.payment.test(this.token).subscribe(data => console.log(data));
  }

  toggleCard() {
    this.turnCard = !this.turnCard;
  }

  focusIn() {
    this.turnCard = true;
  }

  focusOut() {
    this.turnCard = false;
  }

  getBrand() {
    return;
    const cardNumber = this.cardForm.get('cardNumber').value;
    if (cardNumber.length > 5) {
      if (this.cardNumber.substr(0, 6) !== cardNumber.substr(0, 6)) {
        this.cardNumber = cardNumber.substr(0, 6);
        this.payment.getByBin(this.cardNumber);
      }
    } else {
      this.cardNumber = '';
      this.card = undefined;
    }
  }

  checkBank(searchText: string): boolean {
    if (this.card && this.card.bank) {
      return this.card.bank.includes(searchText.toUpperCase());
    }
  }

  submit() {
    console.log(this.cardForm.value);
    const transaction = this.formatSubmit();
    console.log(transaction);
    this.payment.createTransaction(transaction).subscribe(data => this.transacao = data);
  }

  private formatSubmit(): any {
    const transaction = { ...this.cardForm.value };
    if (transaction.installments === '' || transaction.installments === '1') {
      delete transaction.installments;
    } else {
      transaction.installments = +transaction.installments;
    }
    transaction.expirationMonth = +transaction.expirationMonth;
    transaction.expirationYear = +transaction.expirationYear;
    transaction.amount = +transaction.amount;
    return transaction;
  }

  stateChange(event) {
    console.log(event);
  }

  ready(event) {
    console.log(event);
    // setTimeout(() => event.target.playVideo(), );
  }

}
