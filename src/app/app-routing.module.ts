import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TesteComponent } from './teste/teste.component';
import { CursosComponent } from './cursos/cursos.component';
import { CursosDetalheComponent } from './cursos-detalhe/cursos-detalhe.component';
import { AulasComponent } from './aulas/aulas.component';
import { HomeComponent } from './home/home.component';


const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full'},
  { path: 'home', component: HomeComponent },
  { path: 'teste', component: TesteComponent },
  { path: 'cursos', component: CursosComponent },
  { path: 'cursos/:id', component: CursosDetalheComponent },
  { path: 'cursos/:id/:yid', component: CursosDetalheComponent },
  { path: 'aulas/:id', component: AulasComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
