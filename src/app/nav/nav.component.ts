import { Component, OnInit } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import { MatDialog } from '@angular/material/dialog';
import { LoginDialogComponent } from './login-dialog/login-dialog.component';
import { AuthService } from '../core/firebase/auth/auth.service';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})
export class NavComponent implements OnInit {

  user;
  token;
  isAdmin: boolean;
  isLoggedIn$: Observable<boolean> = this.auth.isLoggedIn();
  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
  .pipe(
    map(result => result.matches),
    shareReplay()
  );

  constructor(private breakpointObserver: BreakpointObserver,
              private auth: AuthService,
              public dialog: MatDialog) { }

  ngOnInit(): void {
    this.auth.user$.subscribe(user => {
      console.log(user);
      this.user = user;
    });
    this.auth.token$.subscribe(token => {
      console.log(token);
      this.token = token;
    });
    this.auth.isAdmin().subscribe(res => this.isAdmin = res);
  }

  openLogin(): void {
    const dialogRef = this.dialog.open(LoginDialogComponent, {
      width: '500px'
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      // console.log(result);
      if (result) {
        const { login } = result;
        const { email, password, confirm } = result.form;

        if (login) {
          console.log(email, password);
          this.auth.login(email, password).subscribe(
            success => console.log(success),
            err => console.log(err)
          );
        }

        if (!login && password === confirm) {
          this.auth.register(email, password).subscribe(
            success => console.log(success),
            err => console.log(err)
          );
        }
      }
    });
  }

  logout() {
    this.auth.logout();
  }

}
